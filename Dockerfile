FROM bandlandia/jdk9-oracle
MAINTAINER bandlandia

ENV APPS_DIR=/opt

ARG MAVEN_VERSION=3.5.3
ARG USER_HOME_DIR="/opt"
ARG BASE_URL="http://apache.mirrors.ionfish.org/maven/maven-3/${MAVEN_VERSION}/binaries"

RUN wget -qO- ${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz | tar -xvz -C /opt \
    && ln -s ${APPS_DIR}/apache-maven-${MAVEN_VERSION}/bin/mvn /usr/bin/mvn \
    && ln -s ${APPS_DIR}/apache-maven-${MAVEN_VERSION} /opt/maven


ENV MAVEN_HOME="/${APPS_DIR}/maven"
ENV MAVEN_CONFIG="/opt/.m2"

COPY mvn-entrypoint.sh /usr/local/bin/mvn-entrypoint.sh
COPY settings-docker.xml ${MAVEN_HOME}/ref/

RUN mvn --version
